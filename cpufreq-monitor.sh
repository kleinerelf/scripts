#/bin/bash

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

CPU_LABEL_COL=1
CPU_FREQ_COL=13

MIN_CPU=1200
MAX_CPU=3200

function repeat_char () {
    for (( i = 0; i < ${2}; i++ ));
    do
        echo -n "${1}"
    done
}

function cpufreq_monitor () {
    CPU=$(sudo cpupower monitor | cut --delimiter='|' --fields=${CPU_LABEL_COL} --output-delimiter=' ')
    CPU=(${CPU//[!0-9 ]/})
    CPU_FREQ=$(sudo cpupower monitor | cut --delimiter='|' --fields=${CPU_FREQ_COL} --output-delimiter=' ')
    CPU_FREQ=(${CPU_FREQ[@]//[!0-9 ]/})

    for i in ${!CPU[@]};
    do
        echo -n "CPU: ${CPU[${i}]} ["

        FREQ=${CPU_FREQ[${i}]}
        FILL=$(( 50 * (FREQ - MIN_CPU) / (MAX_CPU - MIN_CPU) ))
        REST=$(( 50 - FILL ))

        repeat_char "*" ${FILL}
        repeat_char " " ${REST}

        echo "] ${FREQ} MHz"
    done
}

function usage () {
    echo "Usage: cpufreq-monitor.sh [OPTIONS]"
    echo "Before any usage, consult cpupower monitor!"
    echo "Options:"
    echo "-l | --label : give the column for the CPU labels"
    echo "-c | --cpu : give the column for the CPU frequency"
    echo "-m | --min : minimum CPU frequency (see cpupower frequency-info)"
    echo "-M | --max : maximum CPU frequency (see cpupower frequency-info)"
    echo "-h | --help : shows this help"
}

while [ "$1" != "" ]; do
    case $1 in
        -l | --label )
            shift
            CPU_LABEL_COL=$1
            ;;
        -c | --cpu )
            shift
            CPU_FREQ_COL=$1
            ;;
        -m | --min )
            shift
            MIN_CPU=$1
            ;;
        -M | --max )
            shift
            MAX_CPU=$1
            ;;
        -h | --help )
            usage
            exit
            ;;
        * )
            ;;
    esac
    shift
done

cpufreq_monitor
