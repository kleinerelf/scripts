#!/bin/bash -i

red=`tput setaf 1`
green=`tput setaf 2`
yellow=`tput setaf 3`
reset=`tput sgr0`

yesno () {
    local msg=${1:-"Do you want to install/update this program?"}
    local option_list=(
        "Yes"
        "No"
    )

    echo "${green}${msg}${yellow}"

    select option in ${option_list[@]};
    do
        case ${option} in
            ${option_list[0]} )
                make install; break
                return 1
                ;;
            ${option_list[1]} )
                return 0
                ;;
            *)
                clear
                echo "${red}Try again.${reset}"
                ;;
        esac
    done

    echo "${reset}"
}

for dir in $(find . -name ".git");
do
    cd ${dir%/*}
    echo "${green}====>$(basename ${dir%/*})"

    echo -n "|---"
    # http://stackoverflow.com/a/3278427
    git remote update
    local=$(git rev-parse @)
    remote=$(git rev-parse @{u})

    if [ ${local} != ${remote} ];
    then
        echo "${yellow}|---Need to pull and update.${reset}"
        git pull -v

        # Ask yes/no Question
        yesno
        if [ "${?}" -eq 1 ];
        then
            makepkg -sri
        fi
    else
        echo "${green}|---Nothing to do.${reset}"
    fi
    cd -
done
